import produce from "immer";
import { loadFromLocal, saveToLocal } from "@/utilities/App/Storage.js";

export const CURRENT_VERSION = '0.3.7'

export const UPDATE_NOTES = `
version ${CURRENT_VERSION} notes

Ability to name builds.
`

const getDefaultState = () => {
    //load from local to see if showing tutorial needed
    let localTutorial = loadFromLocal('showTutorial');
    let promptTutorial = localTutorial !== undefined ? localTutorial : true;
    if (localTutorial === undefined) {
        localStorage.removeItem('Builds')
        localStorage.removeItem('StackDelays')
    }
    saveToLocal(promptTutorial, 'showTutorial')

    //load from local to see if update notes needed
    // let localVersion = loadFromLocal('version');
    // let promptUpdateNotes = localVersion !== CURRENT_VERSION ? true : false;
    // saveToLocal(CURRENT_VERSION, 'version')
    let promptUpdateNotes = false;

    //decide wether the window first shows up
    let show = (promptTutorial || promptUpdateNotes) ? true : false;

    return {
        show: {
            show: show,
            fade: false
        },
        popup: {},
        activeTab: 'Builds',
        promptTutorial: promptTutorial,
        promptUpdateNotes: promptUpdateNotes
    }
}
const App = produce((draft, action) => {
    switch (action.type) {
        case 'SET_SHOW': {
            draft.show.show = action.show
            draft.show.fade = action.fade
            break;
        }
        case 'SET_TAB': {
            draft.activeTab = action.tab
            break;
        }
        case 'SET_SHOWARROW': {
            draft.showArrow = action.showArrow
            break;
        }
        case 'SEND_MESSAGE': {
            draft.popup.message = action.message;
            draft.popup.show = true;
            draft.popup.messageID = draft.popup.messageID !== undefined ? draft.popup.messageID + 1 : 0;
            draft.popup.persist = action.persistent;
            draft.popup.wide = action.wide;
            break;
        }
        case 'HIDE_POPUP': {
            draft.popup.show = false;
            break;
        }
        case 'SET_TUTORIALMODE': {
            draft.tutorialMode = action.mode;
            break;
        }
        case 'SET_PROMPT_TUTORIAL': {
            draft.promptTutorial = action.prompt;
            break;
        }
        default:
            return;
    }
},
    getDefaultState()
)

export default App;