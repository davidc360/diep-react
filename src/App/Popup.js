import React, { Component } from "react";
import Classes from "./Popup.scss";
import { connect } from "react-redux";

import { hidePopup } from "./Actions";

class Popup extends Component {
    popupTimer;

    componentDidUpdate() {
        clearTimeout(this.popupTimer)
        if (!this.props.popup.persist)
            this.popupTimer = setTimeout(() => {
                this.popupRef.current.classList.remove(Classes.showMessage)
                this.props.hidePopup();
            }, 2000)
    }

    _closeClick = (e) => {
        this.props.hidePopup();
    }

    render() {
        return <div className={`${this.props.popup.show ? this.props.popup.persist ? Classes.showMessagePersist : Classes.showMessage : ""} ${Classes.popup} ${this.props.popup.wide ? Classes.wide : ''}`}
            ref={this.popupRef = React.createRef()}
            key={Math.random()}
            onClick={this._closeClick}>
            <div className={`${Classes.popupText} ${this.props.popup.wide ? Classes.wide : ''}`}>
                <div className={Classes.fixedWrapper}>
                    <div className={Classes.close}
                        onClick={this._closeClick}>
                        X</div>
                </div>
                {this.props.popup.message}
            </div>
        </div >
    }
}

const mapStateToProps = (state) => {
    return {
        popup: state.App.popup,
    }
}

const mapDispatch = {
    hidePopup
}

export default connect(mapStateToProps, mapDispatch)(Popup);