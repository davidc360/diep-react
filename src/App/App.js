import React, { Component } from "react";
import { connect } from "react-redux";
import Classes from "./App.scss";
import { UPDATE_NOTES } from "./AppReducer.js";

import NavBar from "../components/NavBar/NavBar";
import BuildsContainer from "../components/Upgrades/UpgradesContainer";
import SettingsContainer from "../components/Settings/SettingsContainer";
import ShortcutsContainer from "../components/Shortcuts/ShortcutsContainer";
import AboutContainer from "../components/About/AboutContainer";
import ScrollArrow from "@/App/ScrollArrow";
import Tutorial from "./Tutorial";

import { setShow, sendMessage } from './Actions';

import { checkIfScrollNeeded } from "@/utilities/App/Scroll";
import { inputs, holdK } from "../utilities/Diep/Inputs";

class App extends Component {
  state = {
    scrollTop: 0,
    activeTab: this.props.activeTab
  }
  refList = {
    Builds: React.createRef(),
    Shortcuts: React.createRef(),
    Settings: React.createRef(),
    About: React.createRef(),
  }
  componentDidMount() {
    if (this.props.promptUpdateNotes && !this.props.promptTutorial) {
      this.props.sendMessage(UPDATE_NOTES, true, true)
    }

    if (this.state)
      window.addEventListener("keydown", this._toggleWindow);

    this.nv.current.addEventListener('mousemove', this._handleMouseMove)

    if (this.props.hotkeyF) {
      window.addEventListener('keydown', this._handleHotkeys)
    }
  }

  componentWillUnmount() {
    // Make sure to remove the DOM listener when the component is unmounted.
    window.removeEventListener("keydown", this._toggleWindow);

    this.nv.current.removeEventListener('mousemove', this._handleMouseMove);

    window.removeEventListener('keydown', this._handleHotkeys)
  }

  shouldComponentUpdate(nextProps, nextState) {
    // if (nextProps.activeTab !== this.props.activeTab) {
    //   console.log(nextProps.activeTab)
    //   this.refList[this.props.activeTab].current.classList.add(Classes.hide)
    //   this.refList[nextProps.activeTab].current.classList.remove(Classes.hide)
    // }
    if (nextState.showArrow !== this.state.showArrow)
      return true;
    if (nextState.scrollTop !== this.state.scrollTop)
      return true;
    if (nextProps.show !== this.props.show) {
      if (nextProps.show.show) {
        clearTimeout(this.addHideTimer);
        this.nv.current.classList.remove(Classes.fade, 'hide')
      } else {
        if (nextProps.show.fade) {
          this.nv.current.classList.add(Classes.fade);
          this.addHideTimer = setTimeout(() => {
            this.props.setShow(false, false)
          }, 300)
        } else {
          this.nv.current.classList.remove(Classes.fade);
          this.nv.current.classList.add('hide')
        }
      }
    }

    if (nextProps.hotkeyF !== this.props.hotkeyF) {
      if (nextProps.hotkeyF) {
        window.removeEventListener('keydown', this._handleHotkeys);
        window.addEventListener('keydown', this._handleHotkeys)
      }
      else {
        window.removeEventListener('keydown', this._handleHotkeys);
      }
    }
    if (nextProps.promptTutorial !== this.props.promptTutorial)
      return true;
    if (nextProps.tutorialMode !== this.props.tutorialMode)
      return true;
    return (nextProps.activeTab !== this.props.activeTab)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.activeTab !== prevState.activeTab)
      return {
        scrollTop: 0,
        activeTab: nextProps.activeTab
      }
    else
      return {}
  }

  componentDidUpdate(prevProps, prevState) {
  }

  _toggleWindow = (ev) => {
    if (ev.key === 'q') {
      this.props.setShow(!this.props.show.show, false)
    }
  }

  _handleMouseMove = (ev) => {
    inputs.mouse(ev.clientX, ev.clientY)
  }

  _handleHotkeys = (e) => {
    switch (e.key) {
      case 'f': {
        holdK();
        break;
      }
      default: return;
    }
  }

  _handleScroll = (e) => {
    this.setState({
      scrollTop: e.target.scrollTop, showArrow: (checkIfScrollNeeded(e.target))
    })
  }

  checkShowArrow = (target) => {
    this.setState({
      showArrow: (checkIfScrollNeeded(target))
    })
  }


  _handleArrowClick = () => {
    this.setState({
      scrollTop: this.state.scrollTop + this.nv.current.clientHeight
    })
  }


  render() {
    return (
      // 
      <div className={`${Classes.App} ${this.props.show.show ? '' : 'hide'} ${this.props.tutorialMode ? 'disableMouse' : ''}`}
        ref={this.nv = React.createRef()}
      >
        <div>
          <NavBar />
        </div>
        <div className={Classes.mainView}>
          <BuildsContainer
            onscroll={this._handleScroll}
            scrollTop={this.state.scrollTop}
            checkShowArrow={this.checkShowArrow} />

          {this.props.activeTab === 'Shortcuts' ?
            <ShortcutsContainer
              onscroll={this._handleScroll}
              scrollTop={this.state.scrollTop}
              checkShowArrow={this.checkShowArrow} />
            : null}

          {this.props.activeTab === 'Settings' ? <SettingsContainer
            onscroll={this._handleScroll}
            scrollTop={this.state.scrollTop}
            checkShowArrow={this.checkShowArrow} />
            : null}

          {this.props.activeTab === 'About' ? <AboutContainer
            onscroll={this._handleScroll}
            scrollTop={this.state.scrollTop}
            checkShowArrow={this.checkShowArrow} />
            : null}

        </div>

        <ScrollArrow show={this.state.showArrow}
          target={this.refList[this.props.activeTab]}
          onclick={this._handleArrowClick} />

        {
          this.props.promptTutorial ?
            <Tutorial />
            : null
        }
      </div >
    );
  }
}

const mapStateToProps = (state) => {
  return {
    show: state.App.show,
    // activeTab: state.App.activeTab,
    activeTab: state.App.activeTab,
    fade: state.App.fade,
    hotkeyF: state.Settings ? state.Settings.hotkeys ? state.Settings.hotkeys['F'] : null : null,
    promptTutorial: state.App.promptTutorial,
    tutorialMode: state.App.tutorialMode,
    promptUpdateNotes: state.App.promptUpdateNotes
  }
}

const mapDispatchToProps = {
  setShow, sendMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(App);