import React, { Component } from "react";
import { connect } from "react-redux";
import PopupClasses from "./Popup.scss";
import Classes from "./Tutorial.scss";

import SkillRowClasses from "@/components/Upgrades/components/SkillRow/SkillRow.scss";
import CommonClasses from "@/components/_Common/Common.scss"
import CardClasses from "@/components/_Common/Card.scss"
import UpgradeClasses from "@/components/Upgrades/components/Upgrades.scss";
import UpgradesContainer from "@/components/Upgrades/UpgradesContainer.scss";
import ButtonClasses from "@/components/NavBar/Buttons.scss";
import ReloadSelectClasses from "@/components/Shortcuts/components/ReloadSelect/ReloadSelect.scss";
import tankClasses from "@/components/Shortcuts/components/TankCard/TankCard.scss";

import { sendMessage, hidePopup, setTutorialMode, setTab, setPromptTutorial } from './Actions';
import { addBuild } from "@/components/Upgrades/Actions";

import { saveToLocal } from "@/utilities/App/Storage.js";

class Tutorial extends Component {
    componentDidMount() {
        this.props.setTutorialMode(true)
        this.playRef.current.addEventListener('click', () => {
            this.showTutorial();
            this.props.setPromptTutorial(false);
        })
    }

    showTutorial = () => {
        saveToLocal(false, 'showTutorial')
        let stepCount = 0;
        let lastStepTime = 0;
        let editOn = false;
        let timeoutHolder = [];
        let tutorialSteps = [
            () => {
                this.props.sendMessage('Left click anywhere to display next tutorial message, press ESC anytime to end tutorial.', true)
            },
            () => {
                this.props.sendMessage(`Press \'Q\' to toggle the extension window. 
                (currently disabled during tutorial)`, true)
            },
            () => {
                this.props.addBuild();
                this.props.addBuild();
                upgrades[0].classList.add(CardClasses.flipIn)
                upgrades[1].classList.add(CardClasses.flipIn)

                setTimeout(() => {
                    upgrades[0].classList.remove(CardClasses.flipIn)
                    upgrades[1].classList.remove(CardClasses.flipIn)
                }, 600)
                this.props.sendMessage('These are editable upgrades cards.', true)
            },
            () => {
                editButton[0].click();

                editButton[0].classList.add(CommonClasses.zoomAwayEffect)
                setTimeout(() =>
                    editButton[0].classList.remove(CommonClasses.zoomAwayEffect), 400)

                editOn = true;

                this.props.sendMessage('Turn on edit mode to edit your builds.', true)
            },
            () => {
                this.props.sendMessage('Press the buttons to add your stats, or use the number row on your keyboard.', true)

                for (let i = 0; i < 20; i++) {
                    let randomIndex = Math.floor(Math.random() * (4) + 3);
                    timeoutHolder.push(setTimeout(() =>
                        simulateSkillPointClick(buttons[randomIndex]), i * 100))
                }
                for (let i = 0; i < 20; i++) {
                    let randomIndex = Math.floor(Math.random() * (8) + 8);
                    setTimeout(() =>
                        simulateSkillPointClick(buttons[randomIndex]), i * 100)
                }
                lastStepTime = new Date();
            },
            () => {
                timeoutHolder.forEach((elem) => clearTimeout(elem))
                timeoutHolder = [];

                this.props.sendMessage('Press backspace to delete last entered stat.', true);

                let e = new Event('keydown');
                e.key = 'Backspace';
                for (let i = 0; i < 10; i++) {
                    timeoutHolder.push(setTimeout(() => {
                        upgrades[0].dispatchEvent(e)
                    }, i * 200
                    ))
                }
            },
            () => {
                timeoutHolder.forEach((elem) => clearTimeout(elem))
                this.props.sendMessage('shift + backspace to empty the build.', true);

                let e = new Event('keydown');
                e.key = 'Backspace';
                e.shiftKey = true;

                upgrades[0].dispatchEvent(e)

            },
            () => {
                this.props.sendMessage('backspace on empty build will delete it.', true);

                let e = new Event('keydown');
                e.key = 'Backspace';// 32 is the keycode for the space bar

                upgrades[0].dispatchEvent(e)

            },
            () => {
                this.props.sendMessage('Right click will show text values of the build, you can also edit the values here.', true);

                timeoutHolder = [];

                let e = new Event('contextmenu');

                upgrades[0].dispatchEvent(e)

                setTimeout(() => {
                    for (let i = 0; i < 10; i++) {
                        let randomIndex = Math.floor(Math.random() * (7));
                        timeoutHolder.push(setTimeout(() =>
                            simulateSkillPointClick(buttons[randomIndex]), i * 150))
                    }
                }, 200)
            },
            () => {
                this.props.sendMessage('The stats will fade in colors so you can easily tell which points are applied first.', true)
                timeoutHolder.forEach((elem) => clearTimeout(elem))
                let e = new Event('contextmenu');

                upgrades[0].dispatchEvent(e)

                e = new Event('keydown');
                e.key = 'Backspace';
                e.shiftKey = true;

                upgrades[0].dispatchEvent(e)

                setTimeout(() => {
                    for (let i = 0; i < 7; i++) {
                        for (let b = 0; b < 5; b++) {
                            setTimeout(() =>
                                simulateSkillPointClick(buttons[b]), (i * 5 + b) * 30)
                        }
                    }
                }, 500)
            },
            () => {
                document.getElementsByClassName(ButtonClasses.button)[1].click();
                this.props.sendMessage('Here you can use shortcuts to stack your bullets, or practice stacking manually.', true)
            },
            () => {
                this.props.sendMessage('To stack, choose your reload and then click your tank. If you used a preset build, your reload will automatically be detected.', true)
                let reloadButton = document.getElementsByClassName(ReloadSelectClasses.reloadButton)[5]
                reloadButton.click();
                let delay = 400;
                setTimeout(() =>
                    reloadButton.click()
                    , delay)
                delay += 600;
                setTimeout(() =>
                    reloadButton.click()
                    , delay)
                delay += 600;
                setTimeout(() =>
                    reloadButton.click()
                    , delay)
                delay += 600;
                setTimeout(() =>
                    reloadButton.click()
                    , delay)
                delay += 600;
                setTimeout(() =>
                    reloadButton.click()
                    , delay)
                delay += 600;
                setTimeout(() =>
                    reloadButton.click()
                    , delay)
                delay += 600;
                setTimeout(() =>
                    reloadButton.click()
                    , delay)
            },
            () => {
                this.props.sendMessage('To practice manual timing, select practice, select your reload and then click the tank you want to practice.', true)
            },
            () => {
                //get the element here because will be inside shortcuts window
                tank = document.getElementsByClassName(tankClasses.editDelays)[0];
                let e = new Event('contextmenu');
                tank.dispatchEvent(e)

                this.props.sendMessage('Flip the tanks to enter custom stack delay values if you know what you\'re doing', true)
            },
            () => {
                this.props.sendMessage('Tutorial end.')
                endTutorial();
            }
        ]

        let nextStep = (e) => {
            if (new Date() - lastStepTime > 1000) {
                if (e.isTrusted && tutorialSteps[stepCount]) {
                    tutorialSteps[stepCount]();
                    stepCount++;
                }
            }
        }

        let handleKeydown = (e) => {
            if (e.key === 'Escape')
                endTutorial();
        }
        let endTutorial = () => {
            this.props.setTutorialMode(false)
            this.props.hidePopup();
            window.removeEventListener('click', nextStep)
            window.removeEventListener('keydown', handleKeydown);
            if (editOn) editButton[0].click()

            if (tank) {
                let e = new Event('contextmenu');
                tank.dispatchEvent(e)
            }
        }
        this.props.setTab('Builds');
        let upgrades = document.getElementsByClassName(UpgradeClasses.upgradesCardContainer);
        let buttons = document.getElementsByClassName(`${SkillRowClasses.skillPoint} ${CommonClasses.clickable}`);
        let tank;
        let editButton = document.getElementsByClassName(UpgradesContainer.editButton);
        this.props.setTutorialMode(true)
        this.props.sendMessage('hello', true)
        window.addEventListener('click', nextStep)
        window.addEventListener('keydown', handleKeydown)
    }

    _handleCheckBox = (e) => {
        if (e.target.checked) {
            saveToLocal(false, 'showTutorial')
        } else {
            saveToLocal(true, 'showTutorial')
        }
    }

    render() {
        return (
            <div className={`${Classes.prompt}`}>
                <div>Hey, you're new. Play tutorial?</div>

                <div className={Classes.buttonWrapper}>
                    <div>
                        <div className={`${CommonClasses.button} ${Classes.button}`}
                            onClick={() => {
                                this.props.setPromptTutorial(false);
                                this.props.setTutorialMode(false)
                            }}>
                            No
                    </div>
                    </div >

                    <div >
                        <div className={`${CommonClasses.button} ${Classes.button}`}
                            ref={this.playRef = React.createRef()}>
                            Yes
                        </div>
                    </div>
                </div>

                <div className={Classes.dontShowAgain}>
                    <label style={{ fontSize: '11px' }}>
                        <input type={'checkbox'} className={Classes.checkBox}
                            onChange={this._handleCheckBox}
                        />
                        don't show again
                    </label>
                </div>
            </div>

        )
    }
}

const simulateSkillPointClick = (target) => {
    target.classList.add(SkillRowClasses.skillPointHover)

    target.click()

    setTimeout(() => {
        target.classList.remove(SkillRowClasses.skillPointHover)
    }, 100)
}

const mapDispatch = { addBuild, sendMessage, setTutorialMode, hidePopup, setTab, setTutorialMode, setPromptTutorial }

export default connect(null, mapDispatch)(Tutorial);