import React from "react";
import Classes from "./ScrollArrow.scss";

const ScrollArrow = (props) => {
    // let _handleArrowClick = () => {
    //     console.log(props.target.current.getWrappedInstance())
    //     props.target.current.getWrappedInstance().scrollBy({
    //         top: props.target.current.getWrappedInstance().clientHeight,
    //         left: 0,
    //         behavior: 'smooth'
    //     })
    // }
    return (
        <div className={Classes.arrowContainer}>
            <svg
                className={`${props.show ? "" : Classes.arrowDisappear} ${Classes.arrow}`}
                onClick={props.onclick}
                height='0' viewBox="0 0 8 5">
                <polygon points="0,0 8,0 4,5" />
            </svg>
        </div>
    )
}

export default ScrollArrow;