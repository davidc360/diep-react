export const setShow = (show, fade) => {
    return ({
        type: 'SET_SHOW',
        show: show,
        fade: fade
    })
}

export const setTab = (tab) => {
    return ({
        type: 'SET_TAB',
        tab: tab
    })
}

export const setShowArrow = (showArrow) => {
    return {
        type: 'SET_SHOWARROW',
        showArrow: showArrow
    }
}

export const sendMessage = (message, persistent, wide) => {
    return {
        type: 'SEND_MESSAGE',
        message: message,
        persistent: persistent,
        wide: wide ? wide : false
    }
}

export const hidePopup = () => {
    return {
        type: 'HIDE_POPUP'
    }
}

export const setTutorialMode = (mode) => {
    return {
        type: 'SET_TUTORIALMODE',
        mode: mode
    }
}

export const setPromptTutorial = (prompt) => {
    return {
        type: 'SET_PROMPT_TUTORIAL',
        prompt: prompt
    }
}