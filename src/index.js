import React from 'react';
import ReactDOM from 'react-dom';

import './index.scss';
import App from './App/App';
import StackPractice from "@/components/Shortcuts/components/StackPractice/StackPractice.js";
import Popup from "@/App/Popup.js";

import registerServiceWorker from './registerServiceWorker';

import { Provider } from "react-redux";
import { store } from "./redux/store";

import { polyfill } from "keyboardevent-key-polyfill";

let root = `<div id="root"> </div>`;
document.querySelector('body').insertAdjacentHTML('afterend', root);
HTMLElement.prototype.blur = () => { }

// let root = document.createElement('div');
// root.id = 'root';
// document.body.appendChild(root);

// KeyboardEvent.prototype.key = () => { };
polyfill();

ReactDOM.render(
    <Provider store={store}>
        <React.Fragment>
            <App />

            <StackPractice />

            <Popup />
        </React.Fragment>
    </Provider>, document.getElementById('root'));

registerServiceWorker();