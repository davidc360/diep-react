import * as Util from "./Util";

//reloads 1-7 in ms
let cycles = [1680, 1520, 1400, 1280, 1160, 1080, 960];

//controls flip state
let flipOn = false;

let mouseX, mouseY;

let flipInProgress = false;

let canvas = document.getElementById('canvas');
let height = canvas.height;
let width = canvas.width;

window.addEventListener('resize', function () {
    width = canvas.width;
    height = canvas.height;
});

document.addEventListener('mousemove', function (event) {
    mouseX = event.clientX;
    mouseY = event.clientY;

    if (flipInProgress) {
        Util.inputs.mouse(width - mouseX, height - mouseY);
    }
});

function flip() {
    flipInProgress = true;

    Util.inputs.mouse(width - mouseX, height - mouseY);

    setTimeout(function () {
        flipInProgress = false;
        Util.inputs.mouse(mouseX, mouseY);
    }, 230);
}

export const startFlip = (reload) => {
    let interval = cycles[reload - 1];
    let adjustedInterval = interval;

    flipOn = true;

    let startTime = new Date().getTime();
    let timeLapsed = 0;

    //tests
    // let firstStart = new Date().getTime();
    // let expectedTime =  interval;

    let loop = () => {
        if (!flipOn) {
            return;
        }

        timeLapsed = new Date().getTime() - startTime;
        startTime = new Date().getTime();
        flip();

        adjustedInterval = interval - (timeLapsed - adjustedInterval);
        setTimeout(loop, adjustedInterval);

        // //testing
        // console.log(expectedTime, startTime-firstStart, startTime-firstStart-expectedTime);
        // expectedTime += interval;
    }

    setTimeout(loop, interval);
    setTimeout(Util.inputs.pressKey(69), interval + 100);
}

export const stopFlip = () => {
    flipOn = false;
}