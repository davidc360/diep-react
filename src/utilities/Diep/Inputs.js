/* commonly used meathods only
no feature, i.e., stack */
const errorUndefined = () => {
    console.error("Error: failed to fetch input methods from diep")
}
export class inputs {
    static keyDown = window && window.input ? window.input.keyDown : errorUndefined;

    static keyUp = window && window.input ? window.input.keyUp : errorUndefined;

    static pressKey = (code) => {
        inputs.keyDown(code);
        inputs.keyUp(code);
    }

    static mouse = window && window.input ? window.input.mouse : errorUndefined;
}

export class diepMethods {
    //takes and elements and renders it
    //exmple: ren_ui, false
    //ren_scoreboard
    static render = window && window.input ? window.input.set_convar : errorUndefined;


    //executes a command with parameter
    //i.e., game_stats_build
    static execute = window && window.input ? window.input.execute : errorUndefined;

}

export const holdK = () => {
    inputs.keyDown(75);
    setTimeout(inputs.keyUp, 4000, 75);
}

export const focusInput = () => {
    try {
        window['setTyping'](true)
    }
    catch (err) {
        console.error('Error with typing in Diep.io. Please report this problem on Discord.')
    }
}

export class tools {

}