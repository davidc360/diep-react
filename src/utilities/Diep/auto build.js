import { inputs } from "./Inputs";
import { diepMethods as diep } from "./Inputs";

// builds
export let builds = {
    r: '565656564444485685685688884433232',
    t: '565656564444485685685688884433237',
    y: '565656564444485685685688882223333',
    f: '565656444888484848485656565677777',
    g: '111112323232323888888823237777777',
    h: '238232323238888238238777777711111',
    v: '565656564444485685685888844777777',
    b: '565656564444485685685688884777777',
    n: '565656564444485685685888847777777'
};

let showStats;
export const applyBuild = (build) => {
    diep.execute("game_stats_build " + build);
    clearTimeout(showStats);
    inputs.keyDown(85);
    showStats = setTimeout(() => {
        inputs.keyUp(85);

        //a delay is needed because if you execute keyUp and set stats at the same time, set stat does not register
        setTimeout(() => {
            diep.execute("game_stats_build " + build);
        }, 50);
    }, 3000);
    return;
}

export const getBuild = (key) => {
    return builds[key]
}

export const checkBuildExist = (key) => {
    return (builds[key] !== undefined) ? true : false
}

export default applyBuild;

