import { inputs } from "./Inputs";
// const setTimeout = (fx, time, argument) =>{
//     let endTime = new Date().getTime() + time;
//     const loop = () => {
//         getTime = new Date().getTime();
//         if (getTime >= endTime){
//             if(argument == 'undefined'){
//                 fx();
//             }else{
//             fx(argument);}
//         }else{
//             setTimeout(loop, 1);
//         }
//     }
//     setTimeout(loop, 1);
// }

export const delayValues = {
    Streamliner: {
        //reload: [1st hold time, 1st delay ... last hold time .... last delay]
        // 5: [280, 100, 230, 150, 220, 210, 130],
        // 7: [210, 90, 180, 150, 160, 180, 80]
        // 7: [200, 80, 170, 140, 150, 170, 70],
        7: [210, 80, 460, 140, 750, 170, 990],

    },
    Pentashot: {
        4: [200, 200, 500],
        7: [200, 200, 470]
    },
    Predator: {
        // 1: [1240, 300, 1070],
        // 2: [1150, 300, 970],
        // 3: [1060, 300, 870],
        // 4: [970, 300, 770],
        // 5: [930, 300, 910],
        // 6: [840, 300, 870],
        // 7: [760, 300, 770]
        5: [930, 300, 1840],
        6: [840, 300, 1710],
        7: [760, 300, 1530]
    },
    // Spreadshot: {
    //     7: [550, 150, 1050, 250, 1600, 350, 2150, 450, 2650]
    // }
}

export const getDelays = ( tankName, reload ) => delayValues[tankName] ?
    delayValues[tankName][reload] ? delayValues[tankName][reload] : undefined : undefined;

//an array of delays, number of bullets the tank shoots
export const stack = ( delays, executionDelay ) => {
    //stack bullets 2~second last
    //minimum key hold duration 10ms
    // let index = 0;

    let minKeyHold = 10;
    let timer = executionDelay ? executionDelay : 0;

    // let convertedDelays = delays.map((item) => Number(item))
    // let convertedDelays = delays;


    let keydownTimestamps = [], keyupTimestamps = [];

    delays.forEach( ( elem, index ) => {
        if ( !( index % 2 ) ) {
            // if (index < delays.length - 1)
            keydownTimestamps.push( elem )
        } else {
            keyupTimestamps.push( delays[index - 1] + elem )
        }
    } );

    keydownTimestamps.pop();
    let lastKeydown = delays[delays.length - 1];

    // console.log(keydownTimestamps, keyupTimestamps, lastKeydown)

    //set a timeout for the entire stack operation to give javascript time to finish up other task, so the timeouts will be more accurate
    setTimeout( () => {
        /* first bullet is at the end of the function, so as the values can queue up first before firing the first shot to optimize accuracy */

        //settimeout 0 allows the cpu to finish other tasks before executing this.
        // setTimeout(inputs.keyDown, 0, 32);
        // inputs.keyDown(32);
        // setTimeout(inputs.keyUp, minKeyHold, 32);

        // let length = (convertedDelays.length - 1) / 2
        // while (length--) {
        //     let delay = convertedDelays[index];
        //     setTimeout(inputs.keyDown, delay, 32);

        //     setTimeout(inputs.keyUp, delay + convertedDelays[index + 1], 32);;
        //     index += 2
        // }

        keydownTimestamps.forEach( elem => {
            setTimeout( inputs.keyDown, elem, 32 );
        } )

        keyupTimestamps.forEach( elem => {
            setTimeout( inputs.keyUp, elem, 32 );
        } )

        setTimeout( inputs.pressKey, lastKeydown, 69 );

        inputs.keyDown( 32 );
        setTimeout( inputs.keyUp, minKeyHold, 32 );

        //stack last bullet, with E to toggle auto fire
        // index++;
        // setTimeout(inputs.pressKey, convertedDelays[index], 69);
    }, timer )
}

export const validateBuild = ( build ) => {
    if ( !build )
        return false;

    for ( let i = 0; i < build.length; i++ ) {
        if ( build[i] === undefined )
            return false
    }

    return true;
}


export const stack2 = ( delays, cycle ) => {
    let cycleCount = 0;
    let index = 0;

    let minKeyHold = 10;
    let keyDownCycle = true;


    //tests
    // let firstStart = new Date().getTime();
    // let expectedTime =  interval;

    //first bullet
    inputs.keyDown( 32 );
    setTimeout( inputs.keyUp, minKeyHold, 32 );

    let startTime = new Date().getTime();
    let timeLapsed = 0;
    let adjustedInterval = delays[index] + 10;

    let loop = () => {
        index++;

        if ( cycleCount >= ( ( cycle - 2 ) * 2 ) ) {
            adjustedInterval = delays[index] - ( timeLapsed - adjustedInterval );
            setTimeout( inputs.pressKey, adjustedInterval, 69 );
            return;
        }

        if ( keyDownCycle ) {
            inputs.keyDown( 32 );
            timeLapsed = new Date().getTime() - startTime;
            setTimeout( loop, delays[index] );
        } else {
            inputs.keyUp( 32 );
            startTime = new Date().getTime();
            adjustedInterval = delays[index] - ( timeLapsed - adjustedInterval );
            setTimeout( loop, adjustedInterval );
        }

        cycleCount++;
        keyDownCycle = !keyDownCycle;
    }
    setTimeout( loop, delays[index] + 10 );
}


