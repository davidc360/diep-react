export const checkIfScrollNeeded = (elem) => {
    if (elem)
        return ((elem.scrollHeight - elem.scrollTop) > elem.clientHeight)
}

export const scrollDown = (target) => {
    target.scrollBy({
        top: target.clientHeight,
        left: 0,
        behavior: 'smooth'
    })
}

