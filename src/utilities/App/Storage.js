export const saveToLocal = (obj, localName) => {
    try {
        let serializedObj;
        // if (typeof obj === 'object')
        serializedObj = JSON.stringify(obj)

        localStorage.setItem(localName, serializedObj ? serializedObj : obj)
    } catch (e) {
        console.error(e);
    }
}

export const loadFromLocal = (localName) => {
    try {
        const serializedObj = localStorage.getItem(localName)
        if (serializedObj === null) return undefined
        return JSON.parse(serializedObj)
    } catch (e) {
        console.error(e);
    }
}

export default { saveToLocal, loadFromLocal }