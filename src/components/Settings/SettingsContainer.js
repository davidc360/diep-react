import React, { Component } from "react";
import { connect } from "react-redux";
import Classes from "./SettingsContainer.scss";
import CommonClasses from "@/components/_Common/Common.scss";

import { resetBuilds } from "@/components/Upgrades/Actions";
import { resetDelays } from "@/components/Shortcuts/Actions.js";
import { setHotkeyF } from "./Actions";

import { checkIfScrollNeeded } from "@/utilities/App/Scroll";
import { setShowArrow, sendMessage } from "@/App/Actions";

class SettingsContainer extends Component {
    state = {

    }
    tabName = 'Settings'
    componentDidMount() {
        if (this.props.activeTab === this.tabName) {
            this.nv.current.scrollTop = this.props.scrollTop;
            setTimeout(() => {
                this.props.checkShowArrow(this.nv.current);
            }, 30);
        }
    }

    componentWillUnmount() {
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.hotkeyF !== this.props.hotkeyF)
            return true;
        if (nextProps.activeTab !== this.props.activeTab)
            if (nextProps.activeTab === this.tabName) {
                this.updateScrollState();
            }
        if (nextProps.scrollTop !== this.props.scrollTop)
            return true;
        return (nextProps.activeTab !== this.props.activeTab);
    }

    componentDidUpdate() {
        if (this.props.activeTab === this.tabName) {
            this.nv.current.scrollTop = this.props.scrollTop;
            this.props.checkShowArrow(this.nv.current);
        }
    }

    updateScrollState = () => {
        this.props.setShowArrow(checkIfScrollNeeded(this.nv.current))
    }

    _handleResetBuildsClick = () => {
        if (window.confirm(
            `Are you sure you want to reset all preset builds?`) === true)
            localStorage.removeItem('Builds');
        localStorage.removeItem('BuildData');
        this.props.resetBuilds();
    }

    _handleResetStackClick = () => {
        if (window.confirm(
            `Are you sure you want to reset all the stack timings?`) === true)
            localStorage.removeItem('StackDelays');
        this.props.resetDelays();
    }


    _handleHotkeyFClick = () => {
        this.props.setHotkeyF(!this.props.hotkeyF);
    }

    render() {
        return (
            <div className={`${Classes.mainContainer} ${this.props.activeTab === this.tabName ? "" : 'hide'}`}
                ref={this.nv = React.createRef()}
                onScroll={this.props.onscroll}>

                <div onClick={this._handleResetBuildsClick}
                    className={`${CommonClasses.button} ${Classes.bigButton}`}>RESET BUILDS</div>

                <div onClick={this._handleResetStackClick}
                    className={`${CommonClasses.button} ${Classes.bigButton} ${Classes.mediumBigText}`}>RESET STACK TIMINGS</div>

                <div onClick={this._handleHotkeyFClick}
                    className={`${CommonClasses.button} ${Classes.bigButton} ${Classes.mediumText} ${Classes.optionButton} ${this.props.hotkeyF ? Classes.optionOn : ""}`}><span className={Classes.optionText}>Press F to level up to lv.45 in sandbox</span>
                    <div>
                        <div className={Classes.toggle}>
                            <input type={'checkbox'} checked={this.props.hotkeyF ? true : false} readOnly={true} />
                        </div>
                    </div>
                </div>

                <div
                    onClick={() => this.props.sendMessage(`sample text`)}
                    className={`${CommonClasses.button} ${Classes.bigButton} ${Classes.mediumBigText}`}>SAMPLE BUTTON</div>
                <div
                    onClick={() => this.props.sendMessage(`one word: nerf overlord`)}
                    className={`${CommonClasses.button} ${Classes.bigButton} ${Classes.mediumBigText}`}>SAMPLE BUTTON</div>
                <div
                    onClick={() => this.props.sendMessage(`hi`)}
                    className={`${CommonClasses.button} ${Classes.bigButton} ${Classes.mediumBigText}`}>SAMPLE BUTTON</div>
            </div>
        )

    }

}

const Toggle = (props) => {
    return <div className={`${Classes.toggle}`}>

    </div>
}

const mapStateToProps = (state) => {
    return {
        activeTab: state.App.activeTab,
        hotkeyF: state.Settings.hotkeys ? state.Settings.hotkeys['F'] : null
    }
}

const mapDispatchToProps = {
    resetBuilds, setHotkeyF, setShowArrow, sendMessage, resetDelays
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsContainer);
