export const setHotkeyF = (hotkeyF) => {
    return {
        type: 'SET_HOTKEYF',
        hotkeyF: hotkeyF,
    }
}