import { loadFromLocal } from "@/utilities/App/Storage";

const getDefaultStates = () => {
    const localHotKeys = loadFromLocal('Hotkeys');

    return {
        hotkeys: localHotKeys
    };
}


export default function Settings(state = getDefaultStates(), action) {
    switch (action.type) {
        case 'SET_HOTKEYF': {
            let hotkeys = { ...state.hotkeys }
            hotkeys['F'] = action.hotkeyF
            return {
                ...state,
                hotkeys: hotkeys
            }
        }
        default:
            return state;
    }
}