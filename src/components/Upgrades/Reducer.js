import { loadFromLocal } from "@/utilities/App/Storage";
import uuidv1 from "uuid/v1";
import produce from "immer";
const initializeDraft = () => {

    const localBuilds = loadFromLocal('Builds');
    const builds = localBuilds ? localBuilds : {};

    const localBuildData = loadFromLocal('BuildData');
    const buildData = localBuildData ? localBuildData : { names: {} };

    return {
        builds: builds,
        buildData: buildData
    };
}


const Upgrades = produce((draft, action) => {
    switch (action.type) {
        case 'ADD_BUILD': {
            let newId = uuidv1();
            draft.builds = { ...draft.builds, [newId]: '' };
            draft.buildSessionData = { ...draft.buildSessionData, [newId]: {} };
            draft.currentFocus = newId

            break;
        }
        case 'FOCUS': {
            draft.currentFocus = action.id

            break;
        }
        case 'DELETE_BUILD': {
            delete draft.builds[action.id];
            delete draft.buildData[action.id];
            break;
        }

        case 'SET_EDIT': {
            draft.edit = action.edit;

            break;
        }
        case 'RESET_BUILDS': {
            return initializeDraft();
        }
        case 'SET_BUILD': {
            let id = action.id;

            draft.builds[id] = action.build

            break;
        }
        case 'SET_CURRENT_RELOAD': {
            draft.currentReload = action.reload

            break;
        }
        case 'SET_NAME': {
            let { id, name } = action;

            draft.buildData.names[id] = name;
        }
        default:
            return;
    }
},
    initializeDraft())

export default Upgrades;