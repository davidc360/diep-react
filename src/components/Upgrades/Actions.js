export const addBuild = () => {
    return {
        type: "ADD_BUILD"
    }
}

export const focusUpgrade = (id) => {
    return {
        type: "FOCUS",
        id: id
    }
}

export const setStatCount = (id, statCount) => {
    return {
        type: "SET_STAT_COUNT",
        id: id,
        statCount: statCount
    }
}

export const deleteBuild = (id) => {
    return {
        type: 'DELETE_BUILD',
        id: id
    }
}

export const setEdit = (edit) => {
    return {
        type: 'SET_EDIT',
        edit: edit
    }
}

export const resetBuilds = () => {
    return {
        type: 'RESET_BUILDS'
    }
}

export const setBuild = (id, build) => {
    return {
        type: 'SET_BUILD',
        id: id,
        build: build
    }
}

export const setFlipped = (id, flipped) => {
    return {
        type: 'SET_FLIPPED',
        id: id,
        flipped: flipped
    }
}

export const setClicked = (id, clicked) => {
    return {
        type: 'SET_CLICKED',
        id: id,
        clicked: clicked
    }
}

export const setCurrentReload = (reload) => {
    return {
        type: 'SET_CURRENT_RELOAD',
        reload: reload
    }
}

export const setName = (id, name) => {
    return {
        type: 'SET_NAME',
        id: id,
        name: name
    }
}


