import React from "react";
import Classes from "./SkillPoint.scss";

import { addStat, focusUpgrade } from "../../Actions";

import { connect } from "react-redux";

const SkillPoint = (props) => {
  // let { background } = props;
  let style = {
    backgroundColor: props.background
  };

  let _clickHandler = props.is_last ?
    (e) => {
      e.stopPropagation();
      props.focusUpgrade(props.index);
      props.addStat(props.id, props.row);
    } : null;

  let output = <div
    // className={`${Classes[`skillPoint--row${props.row}`]} ${Classes.skillPoint}`}
    className={`${Classes.skillPoint}`}
    style={style}
    onClick={_clickHandler}
  />


  return (
    output
  );

}

const mapDispatchToProps = {
  focusUpgrade,
  addStat
}

export default connect(null, mapDispatchToProps)(SkillPoint);
