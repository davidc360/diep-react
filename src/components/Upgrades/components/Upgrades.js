import React, { Component } from "react";
import { SkillRow } from "./SkillRow/SkillRow";

import Classes from "./Upgrades.scss";
import CardClasses from "@/components/_Common/Card.scss";
import * as Autobuild from "@/utilities/Diep/auto build";

import { connect } from "react-redux";

import {
  deleteBuild, setBuild
} from "../Actions";
import { setShow } from "@/App/Actions";
import { setCurrentReload, setName } from "../Actions";

import { focusInput } from "@/utilities/Diep/Inputs.js";

const NUM_OF_ROWS = 8;
let OPA = 0.9;
const SKILL_COLORS = {
  1: [254.5, 170.5, 113.5, OPA],
  2: [251.5, 59.5, 257.5, OPA],
  3: [127.0, 58.0, 256.0, OPA],
  4: [43.0, 106.0, 254.5, OPA],
  5: [254.5, 214.0, 43.0, OPA],
  6: [254.5, 41.5, 43.0, OPA],
  7: [110.5, 253.0, 43.0, OPA],
  8: [43.0, 254.5, 247.0, OPA]
}

//white background values
// const SKILL_COLORS = {
//   1: 'rgba(242, 174, 132,',
//   2: 'rgba(251, 54, 238,',
//   3: 'rgba(160, 76, 240,',
//   4: 'rgba(96, 137, 240,',
//   5: 'rgba(237, 215, 84,',
//   6: 'rgba(253, 82, 94,',
//   7: 'rgba(106, 245, 87,',
//   8: 'rgba(0, 240, 232,'
// }

class Upgrades extends Component {
  state = {
    isClicked: false,
    isFlipped: this.props.isFlipped || false
  }
  componentDidMount() {
    this.statInputRef.current.value = this.props.build ? this.props.build : ""

    this.upgradesRef.current.addEventListener('keydown', this._handleKeypress)
    this.upgradesRef.current.addEventListener('contextmenu', this._handleRightClick)
    this.nv.current.addEventListener('mouseleave', this._handleMouseLeave)

    this.upgradesRef.current.addEventListener('mousedown', this._handleMouseDown)

    this.statInputRef.current.addEventListener('keydown', this._handleTextKeypress)
    this.statInputRef.current.addEventListener('mousedown', this._handleTextMouseDown)
    this.statInputRef.current.addEventListener('contextmenu', this._handleRightClick)
    // this.nv.current.addEventListener('mouseleave', this.unflipCard)
  }

  componentWillUnmount() {
    // Make sure to remove the DOM listener when the component is unmounted.
    this.upgradesRef.current.removeEventListener('keydown', this._handleKeypress)
    this.upgradesRef.current.removeEventListener('contextmenu', this._handleRightClick)

    this.upgradesRef.current.removeEventListener('mousedown', this._handleMouseDown)

    this.statInputRef.current.removeEventListener('keydown', this._handleTextKeypress)
    this.statInputRef.current.removeEventListener('contextmenu', this._handleRightClick)
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.isFlipped !== undefined)
      return {
        isFlipped: nextProps.isFlipped
      }
    else
      return {}
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.isClicked !== this.state.isClicked)
      return true;

    if (nextState.isFlipped !== this.state.isFlipped)
      return true;
    // if (nextProps.build !== this.props.build)
    //   this.statInputRef.current.value = nextProps.build ? nextProps.build : ""


    if (nextProps.editOn !== this.props.editOn)
      return true;

    if (nextProps.name !== this.props.name)
      return true;

    if (nextProps.build !== this.props.build) {
      this.statInputRef.current.value = nextProps.build ? nextProps.build : ""
      return true;
    }

    return false;
  }

  componentDidUpdate() {
    // console.log("Upgrades updated")
  }


  _handleMouseDown = (e) => {
    //check the target to prevent clicking on the add button causing the rows to go down
    if (e.target === this.upgradesRef.current)
      if (!this.flip) {
        // console.log(e.target)
        if (e.button === 0) {
          this.upgradesRef.current.classList.add(Classes.clickedShrink97)
          this.setState({ ...this.state, isClicked: true })
        }
      }
  }
  _handleMouseLeave = (e) => {
    this.upgradesRef.current.classList.remove(Classes.clickedShrink97)
    this.setState({ ...this.state, isClicked: false })
    // this.props.setClicked(this.props.id, false)
  }



  _handleClick = (e) => {
    this.upgradesRef.current.classList.remove(Classes.clickedShrink97)
    // this.props.setClicked(this.props.id, false)

    this.setState({ ...this.state, isClicked: false })

    let target;
    if (!this.props.editOn && this.props.build) {
      target = this.nv.current;

      target.classList.add(CardClasses.disappear)
      setTimeout(() => {
        target.classList.remove(CardClasses.disappear)
      }, 300)

      this.props.setShow(false, true);

      Autobuild.applyBuild(this.props.build);
      let reloadCount = this.state.statCount ? this.state.statCount[7] ? this.state.statCount[7] : 0 : 0;
      this.props.setCurrentReload(reloadCount);
    }
  }

  _handleKeypress = (e) => {
    if (['Backspace'].includes(e.key)) {
      this.deleteStat(e.shiftKey)
    } else if (['1', '2', '3', '4', '5', '6', '7', '8'].includes(e.key)) {
      this.addStat(e.key)
    }
  }

  flipCard = (e) => {
    this.nv.current.classList.add(CardClasses.flip)
    this.upgradesRef.current.classList.add('disableMouse')
    this.statInputRef.current.focus()
  }

  unflipCard = (e) => {
    this.nv.current.classList.remove(CardClasses.flip)
    this.upgradesRef.current.classList.remove('disableMouse')
    //unselect text because rightcliking causes texts to be selected
    this.statInputRef.current.selectionStart = this.statInputRef.current.value.length;
    this.statInputRef.current.selectionEnd = this.statInputRef.current.value.length;
    this.nv.current.focus();
  }

  _handleRightClick = (e) => {
    //for individual skill points to disable shadow

    this.setState({ ...this.state, isFlipped: !this.state.isFlipped })
    if (this.state.isFlipped) {
      this.statInputRef.current.focus()
    }
    else {
      this.statInputRef.current.selectionStart = this.statInputRef.current.value.length;
      this.statInputRef.current.selectionEnd = this.statInputRef.current.value.length;
      console.log("should focus on upgrades")
      this.upgradesRef.current.focus()
    }

    e.preventDefault();
    e.stopPropagation();
    return false;
  }


  _handleTextKeypress = (e) => {
    e.stopPropagation();
  }

  _handleInput = (e) => {
    let cleanedBuild = e.target.value.match(/[1-8]/g);
    let skillCount = {};
    let outputBuild = cleanedBuild;
    if (cleanedBuild) {
      //start off counting first number, because if there's only one number reduce will be skipped
      skillCount[cleanedBuild[0]] = 1;
      outputBuild = cleanedBuild.reduce((acc, item) => {
        if (!skillCount[item]) {
          skillCount[item] = 1;
          return acc += item;
        } else if (skillCount[item] < 7) {
          skillCount[item]++
          return acc += item;
        }
        else {
          return acc
        }
      })
    }
    e.target.value = outputBuild;
    this.props.setBuild(this.props.id, outputBuild)
  }

  _changeBuildName = (e) => {
    this.props.setName(this.props.id, e.target.value)
  }

  addStat = (number) => {
    if (this.props.build !== undefined) {
      if ('12345678'.includes(number)) {
        //if there is no statcount for the build, means it's an empty or new build, or if the stat is less than, and the if the stat length is less than 33
        if (!this.state.statCount || ((this.state.statCount[number] < 7 && (!this.props.build || this.props.build.length < 33)))) {
          let build = this.props.build ?
            this.props.build + number :
            number.toString();

          this.props.setBuild(this.props.id, build)
        }
      }
    }
  }

  deleteStat = (shiftKeyPressed) => {
    let build = this.props.build;
    //if shift key then delete all stats
    if (shiftKeyPressed) {
      build = ''
    }
    else if (build.length > 0) {
      //if build is not empty, delete 1 stat
      build = build.slice(0, -1);
    } else {
      //if build is empty, delete the build and the stat count
      this.props.deleteBuild(this.props.id)
      return;
    }
    this.props.setBuild(this.props.id, build)
  }


  render() {

    let colors = {
      1: [],
      2: [],
      3: [],
      4: [],
      5: [],
      6: [],
      7: [],
      8: [],
    }

    let upgradeRows = [];

    if (this.props.build && this.props.build.length > 0) {
      [...this.props.build].forEach((char, i) => {
        // fade colors by using alpha
        // let alpha = SKILL_COLORS[char][3] - Math.max(0, (i - 8)) * 0.03
        let alpha = SKILL_COLORS[char][3] - (i < 10 ? i * 0.015 :
          i < 20 ? i * 0.022 : i * 0.029)
        let color =
          `rgba(${(SKILL_COLORS[char][0] * alpha + 255 * (1 - alpha)).toFixed(0)},` +
          `${(SKILL_COLORS[char][1] * alpha + 255 * (1 - alpha)).toFixed(0)},` +
          `${(SKILL_COLORS[char][2] * alpha + 255 * (1 - alpha)).toFixed(0)},` +
          `${SKILL_COLORS[char][3]}`
        colors[char].push(color);
      })
    }

    for (let i = 0; i < NUM_OF_ROWS; i++) {
      upgradeRows.push(
        <div className={Classes.skillRowWrapper}
          key={this.props.id + i}
        >
          <SkillRow
            // key={this.props.id + i}
            row={i + 1}
            colors={colors[i + 1]}
            id={this.props.id}
            withShadow={!this.state.isClicked}
            addStat={this.addStat}
          />
        </div>);
    }

    let statCount = {
    }

    for (let i = 0; i < 8; i++) {
      if (colors[i + 1])
        statCount[i + 1] = colors[i + 1].length
    };

    this.setState({ ...this.state, statCount });

    //an extra container for the card is needed to make sure the element doesn't lose mouse focus when it's flipping
    return <div className={Classes.mainContainer}>
      <div className={`${CardClasses.card} ${CardClasses.cardHover} ${this.props.editOn ? Classes.editModeCard : ""} ${Classes.margin10}`}>
        <div className={`${CardClasses.flippableCard} ${this.state.isFlipped ? `${CardClasses.flip} disabledMouse` : ""} ${Classes.upgradesCardContainer}`}
          ref={this.nv = React.createRef()}
        >
          <div className={`${CardClasses.frontside} ${Classes.upgrades}`}
            tabIndex={0}
            ref={this.upgradesRef = React.createRef()}
            onClick={this._handleClick}>
            {upgradeRows}
          </div >
          <textarea className={`${CardClasses.backside} ${Classes.statInput}`} ref={this.statInputRef = React.createRef()}
            maxLength={33}
            onChange={this._handleInput}
          />
        </div>
      </div>
      <input className={`${Classes.nameInput} ${this.props.editOn ? Classes.editModeNameInput : ''}`}
        placeholder={this.props.editOn ? 'enter name' : ''}
        defaultValue={this.props.name}
        onChange={this._changeBuildName}
        onClick={focusInput} />
    </div>
  }
};

const mapState = (state, props) => {
  return {
    // statCount: state.Upgrades.statCount[props.id],
    name: state.Upgrades.buildData.names[props.id]
  }
}

const mapDispatchToProps = {
  deleteBuild,
  setBuild,
  setShow,
  setCurrentReload,
  setName
}

export default connect(mapState, mapDispatchToProps)(Upgrades);
