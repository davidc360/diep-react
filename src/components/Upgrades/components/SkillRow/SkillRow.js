import React from "react";
// import SkillPoint from "../SkillPoint/SkillPoint";
import Classes from "./SkillRow.scss";
import CommonClasses from "@/components/_Common/Common.scss"

import { connect } from "react-redux";
import { addStat, focusUpgrade } from "../../Actions";

const SKILL_POINT_COUNT = 8;


const SkillRow = (props) => {
  let skillRow = [];

  for (let i = 0; i < SKILL_POINT_COUNT; i++) {
    let is_last = i === SKILL_POINT_COUNT - 1;

    skillRow.push(
      <SkillPoint
        row={props.row}
        background={props.colors ? props.colors[i] ? props.colors[i] : null : null}
        key={props.id + props.row + i}
        is_last={is_last}
        id={props.id}
        withShadow={props.withShadow}
        addStat={props.addStat}
      />
    );
  }

  return <div className={`${Classes.skillRow}`}>{skillRow}</div>;
}

/**
Skill point start
**/

const SkillPoint = (props) => {
  // componentDidMount() {
  //   if (this.props.is_last)
  //     this.nv.current.addEventListener('mousedown', this._handleMouseDown)
  // }

  // if (nextProps.clicked !== this.props.clicked) {
  //   if (nextProps.clicked) {
  //     this.nv.current.classList.remove(Classes.shadow)
  //   }
  //   else {
  //     this.nv.current.classList.add(Classes.shadow)
  //   }
  // }

  //   return this.props.background !== nextProps.background
  // }

  let _clickHandler =
    (e) => {
      e.stopPropagation();
      props.addStat(props.row);
    }


  let style = {
    backgroundColor: props.background
  };
  return <div
    className={`${Classes.skillPoint} ${props.is_last ? CommonClasses.clickable : null} ${props.withShadow ? Classes.shadow : ""}`}
    style={style}
    onClick={props.is_last ? _clickHandler : null}
  >
  </div>
}

// const mapStateToProps = (state, props) => {
//   return {

//   }
// }


export { SkillRow };
