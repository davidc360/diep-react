import React from "react";
import Upgrades from "./components/Upgrades";

import Classes from "./UpgradesContainer.scss";
import CommonClasses from "@/components/_Common/Common.scss"
import CardClasses from "@/components/_Common/Card.scss"

// import uuidv1 from 'uuid/v1'
import { connect } from "react-redux";

import { addBuild } from "./Actions";

import { checkIfScrollNeeded } from "@/utilities/App/Scroll";

// const builds = {
//     r: '565656564444485685685688884433232',
//     t: '565656564444485685685688884433237',
//     y: '565656564444485685685688882223333',
//     f: '565656444888484848485656565677777',
//     g: '111112323232323888888823237777777',
//     h: '238232323238888238238777777711111',
//     v: '565656564444485685685888844777777',
//     b: '565656564444485685685688884777777',
//     n: '565656564444485685685888847777777'
// };

class UpgradeContainer extends React.Component {
    tabName = 'Builds';
    state = {
        editOn: false,
    }
    componentDidMount() {
        if (this.props.activeTab === this.tabName) {
            this.UpgradeContainerRef.current.scrollTop = this.props.scrollTop;
            setTimeout(() => {
                this.props.checkShowArrow(this.UpgradeContainerRef.current);
            }, 300);
        }
    }

    componentWillUnmount() {
    }

    shouldComponentUpdate(nextProps, nextState) {
        /** if next active tab is different, update regardless of what the next tab is **/
        if (nextProps.activeTab !== this.props.activeTab)
            return true;

        if (nextProps.activeTab === this.tabName) {

            if (nextProps.scrollTop !== this.props.scrollTop) {
                return true;
            }

            if (nextState.editOn !== this.state.editOn)
                return true;

            // return (this.props.builds !== nextProps.builds)
            return (this.props.builds !== nextProps.builds)
        }
        else {
            return false
        }

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.activeTab === this.tabName) {
            this.UpgradeContainerRef.current.scrollTop = this.props.scrollTop;
            this.props.checkShowArrow(this.UpgradeContainerRef.current);
        }
    }

    _handleClick = (e) => {
        this.props.addBuild();
    }

    _handleEditClick = (e) => {
        this.setState({ ...this.state, editOn: !this.state.editOn })
    }

    render() {
        let upgrades = [];
        if (this.props.builds) {
            upgrades = Object.keys(this.props.builds).map((key) =>
                <Upgrades key={key}
                    id={key}
                    editOn={this.state.editOn}
                    build={this.props.builds[key]} />
            )
        }

        return (
            <div className={`${Classes.mainContainer} ${this.props.activeTab === 'Builds' ? "" : 'hide'}`}>
                <div className={`${Classes.UpgradeContainer}`}
                    ref={this.UpgradeContainerRef = React.createRef()}
                    onScroll={this.props.onscroll}
                >
                    <div className={Classes.fixedWrapper}>
                        <div className={`${CommonClasses.button} ${Classes.editButton}
                        ${this.state.editOn ? Classes.pressedEditButton : ''}`}
                            onClick={this._handleEditClick}>
                            {this.state.editOn ? 'EDIT ON' : 'EDIT'}
                        </div>
                    </div>
                    {upgrades}
                    <div
                        className={`${CardClasses.card} `}
                        onClick={this._handleClick}
                    >
                        <div className={`${CardClasses.grayCard} ${CardClasses.cardHover}`}>
                            ADD
                        </div>
                    </div>
                </div>
            </div >
        )

    }

}

const mapStateToProps = (state) => {
    return {
        showArrow: state.Upgrades.showArrow,
        builds: state.Upgrades.builds,
        activeTab: state.App.activeTab,
    }
}

const mapDispatchToProps = {
    addBuild
}

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(UpgradeContainer);

