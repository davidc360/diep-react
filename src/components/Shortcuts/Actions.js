export const setDelay = (tank, reload, index, value) => {
    return {
        type: 'SET_DELAY',
        tank: tank,
        reload: reload,
        index: index,
        value: value
    }
}

export const trimDelay = (tank, reload, bulletNum) => {
    return {
        type: 'TRIM_DELAY',
        tank: tank,
        reload: reload,
        bulletNum: bulletNum
    }
}

export const showStackPractice = (practiceValues) => {
    return {
        type: 'SHOW_STACK_PRACTICE',
        practiceValues: practiceValues
    }
}

export const hidePractice = () => {
    return {
        type: 'HIDE_STACK_PRACTICE'
    }
}

export const setPracticeSelected = (practiceSelected) => {
    return {
        type: 'SET_PRACTICE_SELECTED',
        practiceSelected: practiceSelected
    }
}

export const resetDelays = () => {
    return {
        type: 'RESET_DELAYS'
    }
}