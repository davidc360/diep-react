import produce from "immer";
import { loadFromLocal } from "@/utilities/App/Storage";

import { delayValues } from "@/utilities/Diep/stacking";

const getDefaultStates = () => {
    const localDelayValues = loadFromLocal('StackDelays');

    return {
        delayValues: localDelayValues ? localDelayValues : delayValues,
    };
}


const Shortcuts = produce((draft, action) => {
    switch (action.type) {
        case 'SET_DELAY': {
            let { delayValues } = draft;

            if (delayValues[action.tank] === undefined) {
                delayValues[action.tank] = {}
            }

            if (delayValues[action.tank][action.reload] === undefined) {
                delayValues[action.tank][action.reload] = [];
            }

            delayValues[action.tank][action.reload][action.index] = Number(action.value);

            break;
        }
        case 'TRIM_DELAY': {
            let { delayValues } = draft;
            let { tank, reload } = action;

            if (!delayValues[action.tank] ||
                !delayValues[action.tank][action.reload])
                return;

            let newArrayLength = action.bulletNum * 2 - 3;
            if (newArrayLength < delayValues[tank][reload].length)
                draft.delayValues[tank][reload] = delayValues[tank][reload].slice(0, newArrayLength);

            break;
        }
        case 'SHOW_STACK_PRACTICE': {
            draft.showPractice = true;

            draft.practiceValues = action.practiceValues;

            break;
        }
        case 'HIDE_STACK_PRACTICE': {
            draft.showPractice = false;

            break;
        }
        case 'SET_PRACTICE_SELECTED': {
            draft.practiceSelected = action.practiceSelected;

            break;
        }
        case 'RESET_DELAYS': {
            return getDefaultStates()
        }
        default:
            return;
    }
},
    getDefaultStates())

export default Shortcuts;