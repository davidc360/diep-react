export const addBuild = () => {
    return {
        type: "ADD_BUILD"
    }
}

export const focusUpgrade = (id) => {
    return {
        type: "FOCUS",
        id: id
    }
}

export const addStat = (id, number) => {
    return {
        type: "ADD_STAT",
        id: id,
        number: number
    }
}

export const setStatCount = (id, statCount) => {
    return {
        type: "SET_STAT_COUNT",
        id: id,
        statCount: statCount
    }
}

export const deleteStat = (id, shift) => {
    return {
        type: 'DELETE_STAT',
        id: id,
        shift: shift
    }
}

export const setEdit = (edit) => {
    return {
        type: 'SET_EDIT',
        edit: edit
    }
}

export const resetState = () => {
    return {
        type: 'RESET_STATE'
    }
}

export const setBuild = (id, build) => {
    return {
        type: 'SET_BUILD',
        id: id,
        build: build
    }
}

export const setFlipped = (id, flipped) => {
    return {
        type: 'SET_FLIPPED',
        id: id,
        flipped: flipped
    }
}

export const setClicked = (id, clicked) => {
    return {
        type: 'SET_CLICKED',
        id: id,
        clicked: clicked
    }
}

export const setReload = (reload) => {
    return {
        type: 'SET_RELOAD',
        reload: reload
    }
}

export const setDetectedReload = (reload) => {
    return {
        type: 'SET_DETECTED_RELOAD',
        reload: reload
    }
}

