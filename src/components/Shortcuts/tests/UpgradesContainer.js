import React from "react";

import ReloadSelect from "../components/ReloadSelect/ReloadSelect";
import TankCard from "../components/TankCard/TankCard";

import Classes from "./UpgradesContainer.scss";
import CommonClasses from "@/components/_Common/Common.scss"
import CardClasses from "@/components/_Common/Card.scss"

// import uuidv1 from 'uuid/v1'
import { connect } from "react-redux";

import { setShowArrow } from "@/App/Actions";
import { addStat, addBuild, deleteStat, setEdit } from "./Actions";

import { checkIfScrollNeeded, updateShowArrow, scrollDown } from "@/utilities/App/Scroll";

import Predator from "@/assets/Diepio/predator.svg";
import Hunter from "@/assets/Diepio/hunter.svg";
import Triplet from "@/assets/Diepio/triplet.svg";
import Twin from "@/assets/Diepio/twin.svg";
import Twinflank from "@/assets/Diepio/twinflank.svg";
import Streamliner from "@/assets/Diepio/streamliner.svg";
// const builds = {
//     r: '565656564444485685685688884433232',
//     t: '565656564444485685685688884433237',
//     y: '565656564444485685685688882223333',
//     f: '565656444888484848485656565677777',
//     g: '111112323232323888888823237777777',
//     h: '238232323238888238238777777711111',
//     v: '565656564444485685685888844777777',
//     b: '565656564444485685685688884777777',
//     n: '565656564444485685685888847777777'
// };

class UpgradeContainer extends React.Component {
    componentDidMount() {
        setTimeout(() => {
            this.updateScrollState();
        }, 600);

        this.UpgradeContainerRef.current.addEventListener('scroll', this.updateScrollState);

        // window.addEventListener('mousewheel', (e) => {
        //     this.UpgradeContainerRef.current.scrollTop -= e.wheelDeltaY;
        // }, false)

        // window.addEventListener('keydown', this._handleKeypress)
    }

    componentWillUnmount() {
        this.UpgradeContainerRef.current.removeEventListener('scroll', this.updateScrollState)
        // window.removeEventListener('keydown', this._handleKeypress)
    }

    shouldComponentUpdate(nextProps) {
        return true;
    }

    componentDidUpdate() {
        if (this.props.activeTab === 'Shortcuts') {
            this.updateScrollState()
        }
    }

    _handleClick = (e) => {
        this.props.addBuild();
    }

    _handleEditClick = (e) => {
        this.props.setEdit(!this.props.edit);
    }

    updateScrollState = () => {
        updateShowArrow(checkIfScrollNeeded(this.UpgradeContainerRef.current));
    }

    render() {
        return (
            <div className={`${Classes.mainContainer} ${this.props.activeTab === 'Shortcuts' ? "" : 'hide'}`}>
                <ReloadSelect />

                <div className={`${Classes.UpgradeContainer}`}
                    ref={this.UpgradeContainerRef = React.createRef()}
                >
                    <TankCard tankType={'Predator'}>
                        <Predator />
                    </TankCard>

                    <TankCard tankType={'Hunter'}>
                        <Hunter />
                    </TankCard>

                    <TankCard tankType={'Triplet'}>
                        <Triplet />
                    </TankCard>

                    <TankCard tankType={'Twin'}>
                        <Twin />
                    </TankCard>

                    <TankCard tankType={'Twinflank'}>
                        <Twinflank />
                    </TankCard>

                    <TankCard tankType={'Streamliner'}>
                        <Streamliner />
                    </TankCard>
                    <div
                        className={`${CardClasses.card} ${CardClasses.cardHover}`}
                        onClick={this._handleClick}
                    >
                        ADD
                    </div>
                </div>
                {/* <img
                    src={require("../../assets/triangle.svg")}
                    className={Classes.arrow}
                /> */}
            </div >
        )

    }

}


const mapStateToProps = (state) => {
    return {
        showArrow: state.Upgrades.showArrow,
        builds: state.Upgrades.builds,
        currentFocus: state.Upgrades.currentFocus,
        activeTab: state.App.activeTab,
        scroll: state.App.scroll,
        edit: state.Upgrades.edit
    }
}

const mapDispatchToProps = {
    addStat,
    addBuild,
    deleteStat,
    setShowArrow,
    setEdit
}

export default connect(mapStateToProps, mapDispatchToProps)(UpgradeContainer);

