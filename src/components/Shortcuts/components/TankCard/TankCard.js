import React, { Component } from "react";
import { connect } from "react-redux";

import Classes from "./TankCard.scss";
import CardClasses from "@/components/_Common/Card.scss";
import TankClasses from "@/assets/Diepio/tank.scss";

import DelayEdit from "./DelayEdit/DelayEdit";

import { setShow, sendMessage } from "@/App/Actions";
import { showStackPractice } from "../../Actions";

import { stack, validateBuild } from "@/utilities/Diep/stacking";


const TANK_BULLET_NUM = {
    Predator: 3,
    Hunter: 2,
    Triplet: 2,
    Twin: 2,
    Twinflank: 2,
    Streamliner: 5
}

const HIGH_PRECISION_TANKS = ['Streamliner', 'Custom'];

class TankCard extends Component {
    componentDidMount() {
        this.frontsideRef.current.addEventListener('contextmenu', this._handleRightClick)
        this.backsideRef.current.addEventListener('contextmenu', this._handleRightClick)
    }

    componentWillUnmount() {
        this.frontsideRef.current.removeEventListener('contextmenu', this._handleRightClick)
        this.backsideRef.current.removeEventListener('contextmenu', this._handleRightClick)
    }

    flipped = false;
    _handleRightClick = (e) => {
        e.preventDefault();
        if (this.flipped) {

            this.nv.current.classList.remove(CardClasses.flip);

            this.frontsideRef.current.classList.remove('disableMouse')
        } else {

            this.nv.current.classList.add(CardClasses.flip)

            this.frontsideRef.current.classList.add('disableMouse')
        }
        this.flipped = !this.flipped;
        return false;
    }

    _handleClick = (e) => {
        let target = e.target;

        if (!this.props.reload) {
            this.props.sendMessage('Please select your reload above.')
        } else if (this.props.delays && this.props.delays[this.props.reload]) {
            let delays = this.props.delays[this.props.reload];

            //make sure there are correct number of delay values
            if ((delays.length === (TANK_BULLET_NUM[this.props.tankType] * 2 - 3) || this.props.tankType === 'Custom') && validateBuild(delays)) {
                if (this.props.practiceSelected) {
                    this.props.showStackPractice(this.props.delays[this.props.reload])
                    this.props.sendMessage('Fire first bullet to begin. Press ESC to stop, right click to reset.')
                }
                else {
                    stack(this.props.delays[this.props.reload], HIGH_PRECISION_TANKS.includes(this.props.tankType) ? 500 : 0)
                }
                target.classList.add(CardClasses.disappear);
                setTimeout(() => {
                    target.classList.remove(CardClasses.disappear)
                }, 300)

                this.props.setShow(false, true);
            }
            else {
                this.props.sendMessage('Invalid combination of delays.')
            }
        } else {
            this.props.sendMessage('This combination has not yet been tested. Please try another tank or reload, or right click on the card and experiment with the delay values. Send the correct values to me and I\'ll push your name of that card permanently.')
        }
    }

    render() {
        return <div className={`${CardClasses.card} ${CardClasses.cardHover}`}>
            <div className={`${CardClasses.flippableCard}`}
                ref={this.nv = React.createRef()}>
                <div onClick={this._handleClick} className={`${CardClasses.frontside} ${CardClasses.disableText} ${CardClasses.grayCard}`}
                    ref={this.frontsideRef = React.createRef()}>
                    <div className={`${TankClasses.tankWrapper} tankWrapper disableMouse`}>

                        {this.props.children ? React.cloneElement(this.props.children, {
                            className: `${TankClasses.tank} disableMouse`
                        }) : null}

                    </div>
                    {this.props.tankType}
                </div>

                <div className={`${CardClasses.backside} ${Classes.editDelays}`}
                    ref={this.backsideRef = React.createRef()}>
                    <DelayEdit tankType={this.props.tankType} bulletNum={TANK_BULLET_NUM[this.props.tankType]} />
                </div>
            </div>
        </div>
    }
}

const mapStateToProps = (state, props) => {
    return {
        activeTab: state.App.activeTab,
        scroll: state.App.scroll,
        reload: state.Upgrades.currentReload,
        delays: state.Shortcuts.delayValues[props.tankType] ? state.Shortcuts.delayValues[props.tankType] : null,
        practiceSelected: state.Shortcuts.practiceSelected
    }
}

const mapDispatchToProps = {
    setShow, sendMessage, showStackPractice
}

export default connect(mapStateToProps, mapDispatchToProps)(TankCard);