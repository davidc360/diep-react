import React, { Component } from "react";
import Classes from "./DelayEdit.scss";
import { connect } from "react-redux";

import { setDelay, trimDelay } from "../../../Actions";

import { focusInput } from "@/utilities/Diep/Inputs.js";

import { loadFromLocal, saveToLocal } from "@/utilities/App/Storage";

class DelayEdit extends Component {
    state = {
        bulletNum: this.props.bulletNum ? this.props.bulletNum : loadFromLocal('CustomCycles') ? loadFromLocal('CustomCycles') : 3,
    }
    // shouldComponentUpdate() {
    //     return true;
    // }
    componentDidUpdate() {
        this.updateInputValues();
    }

    _handleChange = (e) => {
        let indexOffset = 3;
        let input = e.target;
        if (input.value < 0) input.value = 0;
        if (input.value > 9999) input.value = Number(String(input.value).slice(0, 4));

        //if a reload is selected and the value is bigger than 0 (to prevent values like 00 and 000 being reduced to '0')
        if (this.props.currentReload !== undefined && input.value > 0) {
            this.props.setDelay(this.props.tankType, this.props.currentReload, e.target.id - indexOffset, input.value)
        }
    }

    updateInputValues = () => {
        let inputs = this.nv.current.getElementsByTagName('input');
        let indexOffSet = this.props.bulletNum ? 2 : 3;
        for (let i = 0, len = inputs.length - indexOffSet; i < len; i++) {
            if (this.props.delays && this.props.delays[this.props.currentReload] && this.props.delays[this.props.currentReload][i] !== undefined) { inputs[i + indexOffSet].value = this.props.delays[this.props.currentReload][i] } else {
                inputs[i + indexOffSet].value = ""
            }
        }
    }

    _focusHandler = (e) => {
        focusInput();
    }

    _handleInputKeypress = (e) => {
        if (e.key === 'tab')
            if (e.target.nextElementSibling)
                e.target.nextElementSibling.focus()
    }

    InputField = (props) => {
        let isBulletTime = props.index % 2 == 1;
        let text = (() => {
            if (isBulletTime) {
                let index = (props.index + 1) / 2;
                let bulletText;
                switch (index) {
                    case 1:
                        bulletText = '1st';
                        break;
                    case 2:
                        bulletText = '2nd';
                        break;
                    case 3:
                        bulletText = '3rd';
                        break;
                    default:
                        bulletText = index + 'th'
                }
                return bulletText + " bullet"
            } else {
                return 'hold time'
            }
        })();

        return <React.Fragment>
            <div className={Classes.label}>
                {text}
            </div>
            <input className={`${isBulletTime ? Classes.bulletInput : Classes.holdInput} ${Classes.input} ${props.isFirstPair ? Classes.firstPair : ''}`}
                disabled={!this.props.currentReload ? true :
                    props.isFirstPair ? true : false}
                defaultValue={props.defaultValue}
                placeholder={!this.props.currentReload ?
                    'Select reload' :
                    props.defaultValue ? null : 'value in ms'}
                onChange={props.changeHandler}
                onFocus={this._focusHandler}
                onKeyPress={this._handleInputKeypress}
                type='number'
                id={props.index}
            />

        </React.Fragment>
    }
    _handleCycleInput = (e) => {
        let newBulletNum = e.target.value;
        if (newBulletNum >= 2 && newBulletNum <= 10) {
            this.setState({
                bulletNum: newBulletNum
            });
            saveToLocal(newBulletNum, 'CustomCycles')
            this.props.trimDelay(this.props.tankType, this.props.currentReload, newBulletNum)
        }
    }

    CycleInput = (props) => {
        return <React.Fragment>
            <div className={Classes.label}>
                Cycles
            </div>
            <input className={`${Classes.CycleInput} ${Classes.input}`}
                placeholder={'number'}
                onChange={this._handleCycleInput}
                onFocus={this._focusHandler}
                type='number'
            />
        </React.Fragment>
    }


    render() {
        let inputs = [];

        for (let i = 0, len = this.state.bulletNum * 2 - 1; i < len; i++) {
            let defaultValue = (() => {
                if (i === 0)
                    return '0'
                if (i === 1)
                    return '10'
                if (this.props.delays) {
                    if (this.props.delays[this.props.currentReload])
                        return this.props.delays[this.props.currentReload][i - 2]
                }
                return null;
            })();

            let isFirstPair = i === 0 || i === 1 ? true : false;

            inputs.push(<this.InputField
                index={i + 1}
                changeHandler={this._handleChange}
                isFirstPair={isFirstPair}
                key={this.props.tankType + i}
                defaultValue={defaultValue} />)
        }

        return (<div className={Classes.mainContainer}
            ref={this.nv = React.createRef()}
            onScroll={(e) => e.stopPropagation()}>
            <div className={Classes.grid}>
                {
                    this.props.bulletNum ? null :
                        <this.CycleInput />
                }

                {inputs}
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        currentReload: state.Upgrades.currentReload,
        delays: state.Shortcuts.delayValues[props.tankType],
    }
}

const mapDispatchToProps = {
    setDelay, trimDelay
}

export default connect(mapStateToProps, mapDispatchToProps)(DelayEdit);