import React from "react";
import Classes from "./ReloadSelect.scss";
import { connect } from "react-redux";

import CommonClasses from "@/components/_Common/Common.scss"

import { setCurrentReload } from "@/components/Upgrades/Actions";
import { setPracticeSelected } from "@/components/Shortcuts/Actions";

const mapStateToProps = (state, props) => {
    return {
        currentReload: state.Upgrades.currentReload,
        practiceSelected: state.Shortcuts.practiceSelected
    }
}

const mapDispatch = {
    setPracticeSelected
}
const ReloadSelect = (props) => {
    let _handleClick = (e) => {
        props.setPracticeSelected(!props.practiceSelected)
    }

    let PracticeButton = () => {
        return (
            <div className={`${CommonClasses.button} ${Classes.reloadButton} ${props.practiceSelected ? CommonClasses.red : ""}`}
                onClick={_handleClick}>
                Practice
            </div>
        )
    }

    return (
        <div className={Classes.mainContainer}>
            <div className={Classes.prompt}>
                <span className={Classes.text}>Please select your reload:</span>
                <span className={`${Classes.text} ${Classes.autoDetect}`}>Auto detected: {
                    props.currentReload ? props.currentReload : 'N/A'
                }<span>If you used preset upgrades, your reload will be detected automatically.</span></span>
            </div>

            <div className={Classes.reloadButtonGroup}>
                {generateButton(7)}
                <PracticeButton />
            </div>
        </div>
    )
}



const generateButton = (number) => {
    let result = [];
    for (let i = 0; i < number; i++) {
        result.push(
            <Button key={'Shortcuts reload button' + i}>{i + 1}</Button>
        )
    }
    return result;
}

const mapReloadButtonStateToProps = (state, props) => {
    return {
        reload: state.Upgrades.currentReload
    }
}

const mapReloadButtonDispatchToProps = {
    setCurrentReload
}

const Button = connect(mapReloadButtonStateToProps, mapReloadButtonDispatchToProps)((props) => {
    const _handleClick = (e) => {
        props.setCurrentReload(
            e.target.innerText != props.reload ? Number(e.target.innerText) : 0
        )
    }

    return <div className={`${CommonClasses.button} ${Classes.reloadButton} ${props.reload === props.children ? CommonClasses.green : ""}`}
        onClick={_handleClick}>
        {props.children}
    </div>
})

export default connect(mapStateToProps, mapDispatch)(ReloadSelect);