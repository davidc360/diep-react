import React, { Component } from "react";
import { connect } from "react-redux";
import Classes from "./StackPractice.scss";
import { hidePractice } from "../../Actions";
import { setShow } from "@/App/Actions.js";

class StackPractice extends Component {
    state = {
        width: 0,
        progressBarWidths: []
    }
    startFill;

    componentDidMount() {
    }

    componentDidUpdate() {
        window.removeEventListener('mousedown', this._handleMouseDown)
        window.removeEventListener('mouseup', this.handleMouseUp)
        window.removeEventListener('contextmenu', this._handleRightClick)

        if (this.props.showPractice) {
            window.addEventListener('mousedown', this._handleMouseDown)
            window.addEventListener('mouseup', this._handleMouseUp)
            window.addEventListener('contextmenu', this._handleRightClick)
            window.addEventListener('keydown', this._handleKeyDown)
        }
    }

    on = false;
    _handleMouseDown = (e) => {
        if (e.button === 0) {
            let newWidths = this.state.progressBarWidths.slice();
            newWidths.push(0);
            this.setState({
                progressBarWidths: newWidths
            })


            if (!this.on) {
                this.startFill();
                this.startTime = new Date();
                this.on = true;
            }
        }
    }
    _handleMouseUp = (e) => {
        if (e.button === 0) {
            let newWidths = this.state.progressBarWidths.slice();
            newWidths.push(0);
            this.setState({
                progressBarWidths: newWidths
            })
        }
    }
    _handleRightClick = (e) => {
        e.preventDefault();
        clearTimeout(this.progressTimer)
        this.setState({
            progressBarWidths: []
        })
        this.on = false
    }
    _handleKeyDown = (e) => {
        if (e.key === 'Escape') {
            this.props.hidePractice();
            this.props.setShow(true);
            clearTimeout(this.progressTimer)
            this.on = false
        }
    }

    startFill = () => {
        let interval = 35;
        let lastExecuted;
        let elapsedTime = 0;
        let incAmt = interval / this.totalTime;
        let nextTick = (delay) => setTimeout(() => {
            let execTime = new Date();
            let actualDelay = execTime - lastExecuted;
            lastExecuted = execTime;

            let newWidths = this.state.progressBarWidths.slice();
            newWidths[newWidths.length - 1] += incAmt * 100;
            this.setState({
                progressBarWidths: newWidths
            })

            // console.log(elapsedTime);

            elapsedTime += interval;

            if (elapsedTime < this.totalTime) {
                let num = Math.max(Math.min(interval * 2 - actualDelay, interval), 0);
                this.progressTimer = nextTick(num);
            }
        }, delay);
        nextTick(interval);
    }
    render() {

        let { timeValues } = this.props;

        let background = [];
        let progressBars = [];

        if (timeValues && this.props.showPractice) {
            this.totalTime =
                timeValues[timeValues.length - 1];

            let scale = (1 + 170 / this.totalTime);
            background.push(
                <ClickIndicator width={70 / (this.totalTime) * (1 + 170 / this.totalTime) * 100 + '%'} key={'stackfirst'} > </ClickIndicator>
            )
            for (let i = 0, len = timeValues.length; i < len; i++) {
                if (i % 2) {
                    background.push(<ClickIndicator width={timeValues[i] / (this.totalTime) * scale * 100 + '%'} key={'stack' + i} />)
                }
                else {
                    let width = (() => {
                        let current = timeValues[i];
                        for (let b = 0; b < i; b++) {
                            current -= timeValues[b]
                        }
                        return (current - 70) / this.totalTime * scale;
                    })();
                    background.push(<WaitIndicator width={width * 100 + '%'} key={'stack' + i} />)
                }
            }
            background.push(<ClickIndicator width={100 / this.totalTime * scale * 100 + '%'} key={'stacklast'} >hold</ClickIndicator>)



            for (let i = 0, len = this.state.progressBarWidths.length; i < len; i++) {
                progressBars.push(<div className={`${Classes.progressBar} ${i % 2 ? Classes.blue : Classes.red}`} style={{ width: `${this.state.progressBarWidths[i]}%` }} key={'progressDiv' + i} />)
            }
        }
        return (
            <div className={`${Classes.mainContainer}${this.props.showPractice ? '' : 'hide'}`}
                ref={this.nv = React.createRef()}>
                <div className={Classes.guideBars}>
                    {background}
                </div>
                <div className={Classes.progressBarContainer}>
                    {progressBars}
                </div>
            </div>
        )
    }
}

const ProgressBar = (props) => {
    return <div className={Classes.progressBar} style={{ width: '200px' }} />
}

const ClickIndicator = (props) => {
    return (
        <div className={`${Classes.clickIndicator} ${Classes.indicator}`} style={{ width: `${props.width}` }} >{props.children ? props.children : 'click'}</div>
    )
}

const WaitIndicator = (props) => {
    return (
        <div className={`${Classes.waitIndicator} ${Classes.indicator}`}
            style={{ width: `${props.width}` }}>{props.children ? props.children : 'wait'}</div>
    )
}

const mapProps = (state, props) => {
    return {
        showPractice: state.Shortcuts.showPractice,
        timeValues: state.Shortcuts.practiceValues
    }
}

const mapDispatch = {
    hidePractice, setShow
}

export default connect(mapProps, mapDispatch)(StackPractice);