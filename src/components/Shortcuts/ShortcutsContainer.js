import React, { Component } from "react";
import { connect } from "react-redux";

import Card from "@/components/_Common/Card.js";
import CardClasses from "@/components/_Common/Card.scss";
import CommonClasses from "@/components/_Common/Common.scss";
import Classes from "./ShortcutsContainer.scss";

import Predator from "@/assets/Diepio/predator.svg";
import Hunter from "@/assets/Diepio/hunter.svg";
import Triplet from "@/assets/Diepio/triplet.svg";
import Twin from "@/assets/Diepio/twin.svg";
import Twinflank from "@/assets/Diepio/twinflank.svg";
import Streamliner from "@/assets/Diepio/streamliner.svg";
import QuestionMark from "@/assets/Diepio/Question_Mark.svg";

import { stack } from "../../utilities/Diep/stacking";
import { setShow, sendMessage } from "@/App/Actions";

import ReloadSelect from "./components/ReloadSelect/ReloadSelect";
import TankCard from "./components/TankCard/TankCard";

import { checkIfScrollNeeded } from "@/utilities/App/Scroll";
import { loadFromLocal, saveToLocal } from "@/utilities/App/Storage.js";

class ShortcutsContainer extends Component {
    state = {
        pleaseRead: loadFromLocal('pleaseRead') !== undefined ? loadFromLocal('pleaseRead') : true
    }
    tabName = 'Shortcuts'
    componentDidMount() {
        if (this.props.activeTab === this.tabName) {
            this.shortcutsContainer.current.scrollTop = this.props.scrollTop;
            setTimeout(() => {
                this.props.checkShowArrow(this.shortcutsContainer.current);
            }, 30);
        }

        this.tanks = document.getElementsByClassName('tankWrapper');

        window.addEventListener('mousemove', this._handleMouseMove)
    }

    componentWillUnmount() {
        window.removeEventListener('mousemove', this._handleMouseMove)
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.scrollTop !== this.props.scrollTop)
            return true;

        if (nextProps.activeTab !== this.props.activeTab)
            if (nextProps.activeTab === this.tabName) {
                this.updateScrollState();
            }

        if (nextState.pleaseRead !== this.state.pleaseRead)
            return true;
        return nextProps.activeTab !== this.props.activeTab;
    }

    componentDidUpdate() {
        if (this.props.activeTab === this.tabName) {
            this.shortcutsContainer.current.scrollTop = this.props.scrollTop;
            this.props.checkShowArrow(this.shortcutsContainer.current);
        }
    }

    _handleClick = (e) => {
        let target = e.target;

        if (!this.props.reload) {
            this.props.sendMessage('Please select your reload above.', true)
        } else if (stack(e.target.innerText, this.props.reload)) {
            target.classList.add(CardClasses.disappear);
            setTimeout(() => {
                target.classList.remove(CardClasses.disappear)
            }, 300)

            this.props.setShow(false, true);
        } else {
            this.props.sendMessage('This combination is not supported right now. Please try another tank or reload.', true)
        }
    }

    _handleMouseMove = (e) => {
        this.updateTankRotate(e);
    }

    //pass mouse events, because getting mouse position is not possible without listeners and events
    updateTankRotate = (e) => {
        if (this.props.activeTab === 'Shortcuts' && this.props.show)
            if (this.tanks) {
                for (let i = 0; i < this.tanks.length; i++) {
                    let element = this.tanks[i];
                    let viewportOffset = element.getBoundingClientRect();

                    let boxCenter = [viewportOffset.left + viewportOffset.width / 2, viewportOffset.top + viewportOffset.height / 2];

                    let distanceX = e.clientX - boxCenter[0],
                        distanceY = e.clientY - boxCenter[1];
                    let angle = Math.atan2(distanceX, distanceY);
                    let degree = (angle * (180 / Math.PI) * -1) + 90;
                    element.style.transform = ('rotate(' + degree + 'deg)');

                };
            }
    }

    render() {
        return (
            <div className={`${Classes.mainContainer} ${this.props.activeTab === this.tabName ? "" : 'hide'}`}
                ref={this.nv = React.createRef()}>

                <ReloadSelect />

                <div className={Classes.shortcutsContainer}
                    ref={this.shortcutsContainer = React.createRef()}
                    onScroll={this.props.onscroll}>

                    <TankCard tankType={'Predator'}>
                        <Predator />
                    </TankCard>

                    <TankCard tankType={'Hunter'}>
                        <Hunter />
                    </TankCard>

                    <TankCard tankType={'Triplet'}>
                        <Triplet />
                    </TankCard>

                    <TankCard tankType={'Twin'}>
                        <Twin />
                    </TankCard>

                    <TankCard tankType={'Twinflank'}>
                        <Twinflank />
                    </TankCard>

                    <TankCard tankType={'Streamliner'}>
                        <Streamliner />
                    </TankCard>

                    <TankCard tankType={'Custom'}>
                        <QuestionMark />
                    </TankCard>

                </div>

                {this.state.pleaseRead ?
                    <div className={`${Classes.pleaseRead} ${CommonClasses.button}`}
                        onClick={() => {
                            this.setState({ pleaseRead: false })
                            this.props.sendMessage(MESSAGE, true, true)
                            saveToLocal(false, 'pleaseRead')
                        }}>
                        please read
                    </div>
                    : null}
            </div >
        )

    }
}

const MESSAGE = `1.

I included this feature because stacking bullets is relatively easy to do by hand (aside from streamliner) so it won't give you much unfair advantage, if any. 

I've also included a practice mode to help you do it manually if you want.


2.

Due to the sandboxed environment of extensions (Javascript), it\'s not possible to perform an action exactly on time. 

That means you won\'t get a perfect stack every time. And for streamliner, the actions happen very fast and have to be very accurate. So in order to get a stacked streamliner bullet, every action has to execute exactly on time (there\'s 10 values for the streamliner), so you might have to try a lot of times to get a streamliner stack. 

You\'ll get a perfect stack about every couple tries for all other thanks.


(end)`


const mapStateToProps = (state) => {
    return {
        activeTab: state.App.activeTab,
        scroll: state.App.scroll,
        reload: state.Upgrades.reload,
        show: state.App.show.show
    }
}

const mapDispatchToProps = {
    setShow, sendMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(ShortcutsContainer);
