import React from "react";
import Classes from "@/components/_Common/Card.scss";

const Card = (props) => {
    return <div className={`${Classes.card} ${Classes.cardHover} ${props.disableText ? Classes.disableText : ''} ${props.styleName ? props.styleName : ""}`}
        onClick={props.Click}>
        {props.children}
    </div>
}

export default Card;