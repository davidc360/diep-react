import React from "react";

import Predator from "@/assets/Diepio/predator.svg";
import Hunter from "@/assets/Diepio/hunter.svg";
import Triplet from "@/assets/Diepio/triplet.svg";
import Twin from "@/assets/Diepio/twin.svg";
import Twinflank from "@/assets/Diepio/twinflank.svg";
import Streamliner from "@/assets/Diepio/streamliner.svg";

import Classes from "./Credits.scss";
import AboutClasses from "../AboutContainer.scss";
import CommonClasses from "@/components/_Common/Common.scss";

const Credits = (props) => {
    return (
        <div className={Classes.mainContainer}>
            <div className={Classes.heading}>
                Credits
            </div>

            <br />

            <div>
                Huge thanks to
                <span className={CommonClasses.blueUnderline}> Kamuy1337</span> for the awesome tank vectors and toolbar icons!
            </div>

            <div className={Classes.overflow}>
                <WrapperWithSize>
                    <Predator />
                    <Hunter />
                    <Triplet />
                    <Twin />
                    <Twinflank />
                    <Streamliner />
                </WrapperWithSize></div>

        </div>
    )
}

const WrapperWithSize = (props) => {
    let result = props.children.map((item, i) => {
        return React.cloneElement(item, {
            height: '10vh',
            width: '10vh',
            className: AboutClasses.spin,
            key: 'Tank card' + i
        })
    })
    return <React.Fragment>
        {result}
    </React.Fragment>
}


export default Credits;