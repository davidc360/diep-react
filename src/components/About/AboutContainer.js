import React, { Component } from "react";
import { connect } from "react-redux";
import Classes from "./AboutContainer.scss";
import CommonClasses from "@/components/_Common/Common.scss";

import BasicTank from "@/assets/Diepio/basic-tank.svg";
import Predator from "@/assets/Diepio/predator.svg";

import Credits from "./components/Credits";
import { setShowArrow } from "@/App/Actions";

import { CURRENT_VERSION } from "@/App/AppReducer.js";

class AboutContainer extends Component {
    state = {

    }
    tabName = 'About'
    componentDidMount() {
        if (this.props.activeTab === this.tabName) {
            this.nv.current.scrollTop = this.props.scrollTop;
            setTimeout(() => {
                this.props.checkShowArrow(this.nv.current);
            }, 30);
        }
    }

    componentWillUnmount() {
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.scrollTop !== this.props.scrollTop)
            return true;

        return (nextProps.activeTab !== this.props.activeTab);
    }

    componentDidUpdate() {
        if (this.props.activeTab === this.tabName) {
            this.nv.current.scrollTop = this.props.scrollTop;
            this.props.checkShowArrow(this.nv.current);
        }
    }

    _handleClick = () => {
    }
    render() {
        return (
            <div className={`${Classes.mainContainer}`}
                ref={this.nv = React.createRef()}
                onScroll={this.props.onscroll}>

                {/* <img className={`${Classes.spin}`} src={basicTank} height='500px' width='500px' alt={'hi'} /> */}
                <BasicTank className={`${Classes.spin}`} height='50vh' width='50vh' alt={'hi'} />

                <div className={`${Classes.small}`}>version {CURRENT_VERSION}</div>

                <div className={`${Classes.withMargin}`}>This extension is <span className={CommonClasses.blueUnderline}>unnamed</span>, if you have name suggestions submit it to me on Discord vado#8411.</div>

                <div className={`${Classes.withMargin}`}>Bugs, feature requests also contact me.</div>

                <div className={`${Classes.withMargin}`}>Possible future features: <br />
                    - Theme engine <br />
                    - Export presets <br />
                    - Better looking stats panel with vectors
                </div>
                <Credits />

            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        activeTab: state.App.activeTab
    }
}

const mapDispatchToProps = {
    setShowArrow
}

export default connect(mapStateToProps, mapDispatchToProps)(AboutContainer);
