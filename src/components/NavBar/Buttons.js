import React, { Component } from "react";
import Classes from "./Buttons.scss";

import { connect } from "react-redux";
import { setTab } from '@/App/Actions'

const Buttons = (props) => {
    return (
        <div className={Classes.buttonGroup}>
            <Button name={'Builds'} />
            <Button name={'Shortcuts'} />
            <Button name={'Settings'} />
            <Button name={'About'} />
        </div>
    )
}


let mapStateToProps = (state) => {
    return {
        activeTab: state.App.activeTab
    }
}
let mapDispatchToProps = {
    setTab
}

const Button = connect(mapStateToProps, mapDispatchToProps)((props) => {
    let { name } = props;
    let _handleClick = (e) => {
        props.setTab(e.target.innerText)
    }

    return (
        <div
            className={`${Classes.button} ${props.activeTab === name ? Classes.pressed : ''}`}
            onClick={_handleClick}
        >{name}</div>
    )
})

export default Buttons;