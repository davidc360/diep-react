import React from "react";
import Classes from "./Buttons.scss";

const Buttons = () => {
    return (
        <div className={Classes.buttonGroup}>
            <div className={`${Classes.edit} ${Classes.button}`}>Edit</div>
            <div className={`${Classes.settings} ${Classes.button}`}>4 Teams</div>
        </div>
    )
}

export default Buttons;