import React, { Component } from "react";
import Classes from "./NavBar.scss";

import Buttons from "./Buttons";

class Sidebar extends Component {
    render() {
        return (
            <div
                className={Classes.NavBar}>
                {<Buttons />}
            </div>
        )
    }
}

export default Sidebar;