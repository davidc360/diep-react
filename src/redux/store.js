import { createStore, compose, applyMiddleware } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
// import { loadFromLocalStorage } from "@/utilities/App/Storage";

import reducer from "./reducer";
import saveToLocal from "./saveToLocalMiddleware";

// export const store = createStore(reducer, compose(applyMiddleware(saveToLocal), composeWithDevTools()));
export const store = createStore(reducer, compose(applyMiddleware(saveToLocal)));

export default store;