import { saveToLocal } from "@/utilities/App/Storage";

const saveTriggersList = {
    ADD_BUILD: 'Builds',
    DELETE_BUILD: 'Builds',
    SET_BUILD: 'Builds',
    SET_NAME: 'BuildData',
    SET_HOTKEYF: 'Hotkeys',
    SET_DELAY: 'StackDelays',
    TRIM_DELAY: 'StackDelays'
}
const saveTriggers = Object.keys(saveTriggersList);

let lastSaveTime = 0;
const saveRate = 1000;

const saveToLocalMiddleware = ({ getState, dispatch }) => next => action => {
    if (saveTriggers.includes(action.type)) {
        const previousState = getState();
        next(action);
        const nextState = getState();

        switch (saveTriggersList[action.type]) {
            case 'Builds': {
                if (previousState.Upgrades.builds !== nextState.Upgrades.builds)
                    throttledSave(nextState.Upgrades.builds, 'Builds')
                break;
            }
            case 'BuildData': {
                if (previousState.Upgrades.buildData !== nextState.Upgrades.buildData)
                    throttledSave(nextState.Upgrades.buildData, 'BuildData')
                break;
            }
            case 'StackDelays': {
                if (previousState.Shortcuts.delayValues !== nextState.Shortcuts.delayValues)
                    throttledSave(nextState.Shortcuts.delayValues, 'StackDelays')
                break;
            }
            case 'Hotkeys': {
                if (previousState.Settings.hotkeys !== nextState.Settings.hotkeys)
                    throttledSave(nextState.Settings.hotkeys, 'Hotkeys')
                break;
            }
        }
    }
    else {
        next(action);
    }
}

let saveTimer;
const throttledSave = (obj, localName) => {
    // let now = new Date();

    // if (now - lastSaveTime >= saveRate) {
    //     setTimeout(() => {
    //         saveToLocal(obj, localName)
    //     }, saveRate)
    //     lastSaveTime = new Date();
    // }
    clearTimeout(saveTimer);
    saveTimer = setTimeout(() => {
        saveToLocal(obj, localName)
    }, saveRate)
}

// export const clearStorage = () => {
//     let items = Object.keys(saveTriggersList).map(x => saveTriggersList[x]).filter((x, i, a) => a.indexOf(x) == i)
//     console.log(items)
//     items.forEach((item) => localStorage.removeItem(item))
// }

export default saveToLocalMiddleware;