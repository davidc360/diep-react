import App from "@/App/AppReducer";
import Upgrades from "../components/Upgrades/Reducer";
import Shortcuts from "@/components/Shortcuts/Reducer";
import Settings from "@/components/Settings/Reducer";

import { combineReducers } from "redux";

export default combineReducers({
    App,
    Upgrades,
    Shortcuts,
    Settings
})
